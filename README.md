# micropython

Optimised to run on microcontrollers http://micropython.org

* [repology](https://repology.org/project/micropython/versions)
* https://apps.fedoraproject.org/packages/micropython
* https://src.fedoraproject.org/rpms/micropython

## See compiled version https://gitlab.com/debian-packages-demo/micropython
## See not working packaged version https://gitlab.com/alpinelinux-packages-demo/micropython